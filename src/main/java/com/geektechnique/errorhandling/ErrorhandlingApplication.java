package com.geektechnique.errorhandling;

import org.apache.tomcat.util.descriptor.web.ErrorPage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

@SpringBootApplication
public class ErrorhandlingApplication {


    //does not work
//    @Bean
//    public EmbeddedServletContainerCustomizer containerCustomizer(){
//        return ( container -> {
//            ErrorPage custom404Page = new ErrorPage(HttpStatus.NOT_FOUND,"/404");
//            container.addErrorPages(custom404Page);
//        });
//    }

//    @Bean
//    public WebServerFactoryCustomizer customizer(){
//        return ( container ->{
//            ErrorPage custom404Page = new ErrorPage();
//            container.addErrorPages(custom404Page);
//        });
//    }

    //need more work on this- blowing too much time trying to figure it out

    public static void main(String[] args) {
        SpringApplication.run(ErrorhandlingApplication.class, args);
    }

}
